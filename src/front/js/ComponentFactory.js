import React from "react";
import MenuItem from "../components/MenuItem";
import MenuBar from "../components/MenuBar";
import Image from "../components/Image";
import TopNavigationBar from "../components/TopNavigationBar";
import Page from "../components/Page";
import HtmlWidget from "../components/HtmlWidget";

class ComponentFactory {
    static createComponent(componentConfig){
        if(componentConfig.type === "menu_item"){
            return <MenuItem data={ componentConfig } nested={true}/>;
        }
        else if(componentConfig.type === "image"){
            return <Image data={componentConfig}/>;
        }
        else if(componentConfig.type === "menu_bar"){
            return <MenuBar data={componentConfig}/>;
        }
        else if(componentConfig.type === "navigation_bar" && componentConfig.details.location === "top"){
            return <TopNavigationBar data={componentConfig}/>;
        }
        else if(componentConfig.type === "page"){
            return <Page data={componentConfig}/>;
        }
        else if(componentConfig.type === "html"){
            return <HtmlWidget data={componentConfig}/>;
        }
    }
}


export default ComponentFactory;
