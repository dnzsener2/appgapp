
export default class Pipeline {
    id;
    name;
    triggers;
    tasks;




    constructor(config) {
        this.id = config.id;
        this.name = config.name;
        this.triggers = config.start.triggers;
        this.tasks = config.tasks;

    }

    willTrigger(event){
        var trigger = this.triggers.find(trigger=>{
            return trigger.componentName == event.componentName && trigger.componentType == event.componentType  && trigger.event == event.action
        })
        return trigger != null;
    }

    getNextTasks(stepId){
        return this.tasks.filter((task)=>{
            return task.prev == stepId;
        })
    }


}

class PipelineTrigger{
    componentName;
    componentType;
    event;
}
