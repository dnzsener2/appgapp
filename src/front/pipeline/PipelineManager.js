import dispatcher from "../../Dispatcher";
import {EventEmitter} from "events";
import Pipeline from "./Pipeline";

export default class PipelineManager extends EventEmitter {

    pipelines = [];


    constructor(props) {
        super(props);
        props.pipelines.map(pipeline=>{
            this.pipelines.push(new Pipeline(pipeline));
        })
    }

    triggerTaskForPipeline(pipelineToBeTriggered, stepId, action){
        setTimeout(()=>{

            var nextTasks = pipelineToBeTriggered.getNextTasks(stepId);
            nextTasks.map(nextTask=>{
                this.emit(nextTask.componentType, {commandId:{pipelineId:pipelineToBeTriggered.id, step:nextTask.id},
                    componentType:nextTask.componentType,
                    componentName:nextTask.componentName,
                    action:nextTask.action,
                    details:nextTask.details
                });
            })


        }
    )

    }
    handleActions(action) {

        if(action.commandId != null){
            var pipelineId = action.commandId.pipelineId;
            var stepId = action.commandId.step;
            var pipeline = this.pipelines.find(pipeline=>{
                return pipeline.id == pipelineId;
            })

            this.triggerTaskForPipeline(pipeline, stepId, action);
        }
        else{
            var pipelinesToBeTriggered = this.pipelines.filter(pipeline=>{
                return pipeline.willTrigger(action);
            })

            pipelinesToBeTriggered.map(pipelineToBeTriggered=>{

                this.triggerTaskForPipeline(pipelineToBeTriggered, "start", action);

            })
        }

    }
}