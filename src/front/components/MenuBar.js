import {Component} from "react";
import React from "react";
import ComponentFactory from "../js/ComponentFactory";


export default class MenuBar extends Component{

    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light" >
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                     {
                         this.props.data.details.items.map((menuItem)=> {
                             return ComponentFactory.createComponent(menuItem);
                         })
                     }
                    </ul>
                </div>
            </nav>
        );
    }
};

