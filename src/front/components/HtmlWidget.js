import {Component} from "react";

import React from "react";

export default class HtmlWidget extends Component{
    render(){
        const name = JSON.stringify(this.props.data.details.content);

        return(

            <div dangerouslySetInnerHTML={{__html: this.props.data.details.content}} />

        )
    }
};