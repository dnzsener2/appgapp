import {Component} from "react";
import React from "react";

export default class Image extends Component{
    render(){

        return(
            <img className={"img-thumbnail"} src={this.props.data.details.src}></img>
        )
    }
};