import React, { Component } from 'react';
import './../css/bootstrap.css';
import ComponentFactory from "../js/ComponentFactory";
import dispatcher from "../../Dispatcher";

export const MENU_ITEM_ACTIONS = {
    CLICK: 'click'
};

class MenuItem extends React.Component {

    onclick = () => {
        dispatcher.dispatch({
            eventOrigination:'userTrigger', // userTrigger // pipelineTrigger
            componentName: this.props.data.name, // pages3
            data:null,
            action: MENU_ITEM_ACTIONS.CLICK,
            componentType:this.props.data.type //menuitem
        })
    };

    render(){
        if(this.props.data.details.items != null && this.props.data.details.items.length >0){
            return <li  className="nav-item dropdown" >
                {(this.props.data.details.src) && <img className="img-thumbnail" src={this.props.data.details.src} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></img>
                }<a  className="nav-link dropdown-toggle" href="#" onClick={() => this.onclick()} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.props.data.details.label}
            </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    {this.props.data.details.items.map((subNode) => {
                        subNode.nested =  true;
                        return ComponentFactory.createComponent(subNode);
                    })}
                </div>
            </li>
        }else{
            if(this.props.data.nested){
                return <a  className="dropdown-item" href="#" onClick={() => this.onclick()}>{this.props.data.details.label}</a>;
            }
            else{
                return <li className="nav-item ">
                    <a  className="nav-link" href="#" onClick={() => this.onclick()}>{ this.props.data.details.label }</a>
                </li>
            }
        }
    }
};
export  default MenuItem;