import {Component} from "react";
import React from "react";
import ComponentFactory from "../js/ComponentFactory";

export default class Layout extends Component{
    render(){
        const rowDoms = this.props.children.map((row)=>{
            const colDoms = row.map((col)=>{
                const colCompConfig = this.props.layoutConfig[col.item];
                let colDom = ComponentFactory.createComponent(colCompConfig);
                return (
                    <div className={"col-lg-"+col.size}>
                        {colDom}
                    </div>
                )
            });
            return (
                <div className={"row"}>
                    {colDoms}
                </div>
            )
        });
        return(
            <div>
                <div >
                    {rowDoms}
                </div>
            </div>
        )
    }
};
