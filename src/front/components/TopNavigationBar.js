import {Component} from "react";
import React from "react";
import './../css/bootstrap.css';
import Layout from "./Layout";



export default class TopNavigationBar extends Component{

    render(){
        if(this.props.data.details.layout){
            const subComponentConfigMap = {};
            this.props.data.details.items.forEach(item=>{
                subComponentConfigMap[item.name] = item;
            })
            return <Layout layoutConfig={subComponentConfigMap} children={this.props.data.details.layout.rows}/>
        }else{
            return(
                <div > </div>
            )
        }
    }
};
