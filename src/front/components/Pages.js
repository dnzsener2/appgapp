import React, {Component} from "react";
import ComponentFactory from "../js/ComponentFactory";
import dispatcher from "../../Dispatcher";

export default class Pages extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: ""
        }
    }

    componentDidMount() {
        this.props.pipelineManager.on(this.props.data.type, this.updatePageContent);
    }

    updatePageContent = (command) => {
        if(command.componentName == this.props.data.name) {
            this.setState({page: command.details.pageName});
            dispatcher.dispatch({
                commandId:command.commandId,
                componentName:this.props.data.name,
                componentType:this.props.data.type,
                action:"commandExecuted",
                data:{
                    success:true
                }
            })
        }
    };

    render() {

        return (
            <div>
                {this.props.data.details.items.map((page) => {
                    if (this.state.page === page.name) {
                        this.props.data.details.default = "";
                        return ComponentFactory.createComponent(page);
                    }
                })}

                {this.props.data.details.items.map((page) => {
                    if (this.props.data.details.default === page.name) {
                        return ComponentFactory.createComponent(page);
                    }
                })}
            </div>
        );


    }


}