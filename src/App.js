import React, { Component } from 'react';
import TopNavigationBar from "./front/components/TopNavigationBar";
import data from './data/app';
import pipeline from "./front/pipeline/Pipeline";
import dispatcher from "./Dispatcher"
import Pages from "./front/components/Pages";
import PipelineManager from "./front/pipeline/PipelineManager";
class App extends Component {

    pipelineManager;

    constructor(props){
        super(props);
        this.pipelineManager = new PipelineManager({pipelines:data.details.pipelines});
    }
    componentDidMount() {

        dispatcher.register(action=>{
            this.pipelineManager.handleActions(action);
        });

    }

    render() {
      //  const pipe=pipeline(data.details.pipelines)

        const navbarDoms = data.details.navigation_bar.map((navBarConf)=>{
            if (navBarConf.details.location == "top"){
                return (<TopNavigationBar  key={navBarConf.name}  data={navBarConf}/>);
            }
            else{
                return (<div key={navBarConf.name} data={navBarConf}/>);
            }
        });
        const pageDoms=data.details.pages.map(pageConf=>{
            if (pageConf.type == "pages"){
                return (<Pages pipelineManager={this.pipelineManager} data={pageConf}/>);
            }
        });
        return (
            <div>
                {navbarDoms}
                {pageDoms}
           </div>
        )
    }
}
export default App;
